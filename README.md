## Engine (Name TBD)
A simple game engine built on top of SDL2

### Building
* Install [conan](https://conan.io/)
* Add the necessary remotes to conan:
    ```
    conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
    conan remote add chasevedder https://api.bintray.com/conan/chasevedder/conan
    ```
* Build & run the project:

    Unix based:
    ```
    ./build.sh
    ```

    Powershell:
    ```
    ./build.ps1
    ```

### CLion Integration
For using conan with CLion, the [conan plugin](https://blog.jetbrains.com/clion/2019/05/getting-started-with-the-conan-clion-plugin/)
is recommended