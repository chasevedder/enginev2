from conans import ConanFile, CMake


class EngineConan(ConanFile):
    name = "Engine"
    version = "0.1"
    license = "GPL v2"
    author = "Chase Vedder chasevedder@gmail.com"
    url = "https://bintray.com/chasevedder/conan/Engine:chasevedder"
    description = "A game engine based on SDL2"
    topics = ("sdl2", "game engine", "game")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {
        "shared": "True",
        "*:shared": "True"
    }
    generators = "cmake"
    exports_sources = "engine/src/*", "engine/include/*", "engine/CMakeLists.txt"
    requires = (
        "sdl2/2.0.10@chasevedder/conan",
        "sdl2_ttf/2.0.15@bincrafters/stable"
    )


    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="engine")
        cmake.build()

    def package(self):
        self.copy("*.hpp", dst="include", src="engine/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["Engine"]
