//
// Created by Chase on 1/7/2020.
//

#ifndef ENGINE_COMMON_HPP
#define ENGINE_COMMON_HPP

#ifdef _WIN32
    #if defined DLL_EXPORT
        #define DECLDIR __declspec(dllexport)
    #else
        #define DECLDIR __declspec(dllimport)
    #endif
#else
    #define DECLDIR
#endif

#endif //ENGINE_COMMON_HPP
