//
// Created by Chase on 1/2/2020.
//

#ifndef ENGINE_GAME_HPP
#define ENGINE_GAME_HPP

#include <memory>

#include "SDL.h"
#include "core/common.hpp"
#include "../input/mapped_input.hpp"

class Game {
public:
    DECLDIR Game();
    ~Game() = default;

    DECLDIR bool Init(int width, int height);
    DECLDIR void Run();
private:
    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> window;
    std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> renderer;

    void InputTester(MappedInput& input);
};


#endif //ENGINE_GAME_HPP
