//
// Created by Chase on 1/6/2020.
//

#ifndef ENGINE_MAPPED_INPUT_HPP
#define ENGINE_MAPPED_INPUT_HPP

#include <set>
#include "../core/common.hpp"

class MappedInput {
public:
    friend class InputMapper;

    MappedInput() = default;
    ~MappedInput() = default;

    DECLDIR bool GetState(const std::string& state);
    DECLDIR bool GetAction(const std::string& action, bool consume);
private:
    std::set<std::string> states;
    std::set<std::string> actions;
};


#endif //ENGINE_MAPPED_INPUT_HPP
