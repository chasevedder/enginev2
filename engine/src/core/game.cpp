//
// Created by Chase on 1/2/2020.
//

#include "core/game.hpp"

#include "SDL.h"
#include "SDL_ttf.h"
#include "../input/input_mapper.hpp"
#include <iostream>

Game::Game() :
    window(nullptr, SDL_DestroyWindow),
    renderer(nullptr, SDL_DestroyRenderer) {}

bool Game::Init(int width, int height) {
    bool success = true;
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cerr << "Unable to initialize SDL2" << std::endl;
        success = false;
    }

    std::cout << "Successfully initialized SDL2" << std::endl;

    if (TTF_Init() != 0) {
        std::cerr << "Unable to initialize SDL2_ttf" << std::endl;
        success = false;
    }

    std::cout << "Successfully initialized SDL2_ttf" << std::endl;

    if (!success) {
        return false;
    }

    window.reset(SDL_CreateWindow("Pong", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE));
    renderer.reset(SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED));

    SDL_RenderSetLogicalSize(renderer.get(), 1280, 720);

    return !(window == nullptr || renderer == nullptr);

}

void Game::Run() {
    bool quit = false;

    InputMapper mapper;
    auto context = new InputContext();
    context->CreateButtonToStateMapping(SDLK_w, "MoveUp");
    context->CreateButtonToStateMapping(SDLK_s, "MoveDown");
    context->CreateButtonToActionMapping(SDLK_f, "Fire");
    mapper.PushContext(context);
    mapper.AddCallback("test",  std::bind(&Game::InputTester, this, std::placeholders::_1));
    mapper.AddCallback("test2", std::bind(&Game::InputTester, this, std::placeholders::_1));

    while (!quit) {
        SDL_Event e;

        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
            else if (e.type == SDL_KEYDOWN) {
                mapper.SetButtonState(e.key.keysym.sym, true);
            }
            else if (e.type == SDL_KEYUP) {
                mapper.SetButtonState(e.key.keysym.sym, false);
            }
        }

        mapper.Dispatch();
        mapper.Clear();

        SDL_SetRenderDrawColor(renderer.get(), 0x00, 0x00, 0x00, 0xFF);
        SDL_RenderClear(renderer.get());
        SDL_RenderPresent(renderer.get());

        SDL_Delay(10);
    }

    SDL_Quit();
}

void Game::InputTester(MappedInput& input) {
    if (input.GetState("MoveUp")) {
        std::cout << "UpMove" << std::endl;
    }
    if (input.GetState("MoveDown")) {
        std::cout << "MoveDown" << std::endl;
    }
    if (input.GetAction("Fire", true)) {
        std::cout << "Fire" << std::endl;
    }

    // This shouldn't happen
    if (input.GetAction("Fire", true)) {
        std::cout << "FireAgain" << std::endl;
    }
}