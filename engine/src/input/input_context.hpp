//
// Created by Chase on 1/6/2020.
//

#ifndef ENGINE_INPUT_CONTEXT_HPP
#define ENGINE_INPUT_CONTEXT_HPP


#include <string>
#include <SDL_keycode.h>
#include <map>

class InputContext {
public:
    InputContext() = default;
    ~InputContext() = default;

    std::string MapButtonToAction(SDL_Keycode key) const;
    std::string MapButtonToState(SDL_Keycode key) const;

    void CreateButtonToActionMapping(SDL_Keycode key, const std::string& action);
    void CreateButtonToStateMapping(SDL_Keycode key, const std::string& state);
private:
    std::map<SDL_Keycode, std::string> buttonToActionMap;
    std::map<SDL_Keycode, std::string> buttonToStateMap;
};


#endif //ENGINE_INPUT_CONTEXT_HPP
