//
// Created by Chase on 1/6/2020.
//

#include <iostream>
#include "input_mapper.hpp"

// TODO: Require button release before triggering action again
void InputMapper::SetButtonState(SDL_Keycode key, bool pressed) {
    std::string state = MapButtonToState(key);
    std::string action = MapButtonToAction(key);
    bool previouslyPressed = previousMappedInput.GetAction(action, false);

    if (pressed && !previouslyPressed) {
        if (!action.empty()) {
            currentMappedInput.actions.insert(action);
            updated = true;
            return;
        }
    }

    if (pressed) {
        if (!state.empty()) {
            currentMappedInput.states.insert(state);
            updated = true;
            return;
        }
    }

    currentMappedInput.states.erase(state);
    previousMappedInput.actions.erase(action);
}

void InputMapper::AddCallback(const std::string &id, const std::function<void (MappedInput &)>& callback) {
    callbackTable.insert(std::make_pair(id, callback));
}

void InputMapper::RemoveCallback(const std::string &id) {
    callbackTable.erase(id);
}

void InputMapper::Dispatch() {
    for (const auto& callback : callbackTable) {
        callback.second(currentMappedInput);
    }
}

void InputMapper::PushContext(InputContext* context) {
    activeContexts.push_front(context);
}

void InputMapper::Clear() {
    previousMappedInput.states.clear();
    previousMappedInput.states.insert(currentMappedInput.states.begin(), currentMappedInput.states.end());

    previousMappedInput.actions.clear();
    previousMappedInput.actions.insert(currentMappedInput.actions.begin(), currentMappedInput.actions.end());
    currentMappedInput.actions.clear();
}

std::string InputMapper::MapButtonToState(SDL_Keycode key) {
    for (auto context : activeContexts) {
        std::string state = context->MapButtonToState(key);
        if (!state.empty()) {
            return state;
        }
    }

    return "";
}
std::string InputMapper::MapButtonToAction(SDL_Keycode key) {
    for (auto context : activeContexts) {
        std::string action = context->MapButtonToAction(key);
        if (!action.empty()) {
            return action;
        }
    }

    return "";
}
