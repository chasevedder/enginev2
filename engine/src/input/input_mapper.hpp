//
// Created by Chase on 1/6/2020.
//

#ifndef ENGINE_INPUT_MAPPER_HPP
#define ENGINE_INPUT_MAPPER_HPP


#include <string>
#include <set>
#include <list>
#include <SDL_keycode.h>
#include <functional>
#include "input/mapped_input.hpp"
#include "input_context.hpp"

class InputMapper {
public:
    InputMapper() = default;
    ~InputMapper() = default;

    void SetButtonState(SDL_Keycode key, bool pressed);
    void AddCallback(const std::string& id, const std::function<void(MappedInput&)>& callback);
    void RemoveCallback(const std::string& id);
    void Dispatch();
    void PushContext(InputContext* context);
    void Clear();
private:
    std::string MapButtonToAction(SDL_Keycode key);
    std::string MapButtonToState(SDL_Keycode key);

    std::list<InputContext*> activeContexts;
    std::unordered_map<std::string, std::function<void(MappedInput&)>> callbackTable;

    MappedInput currentMappedInput;
    MappedInput previousMappedInput;

    bool updated;
};


#endif //ENGINE_INPUT_MAPPER_HPP
