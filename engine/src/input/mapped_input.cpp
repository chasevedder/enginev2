//
// Created by Chase on 1/6/2020.
//

#include <iostream>
#include "input/mapped_input.hpp"

bool MappedInput::GetState(const std::string &state) {
    return states.find(state) != states.end();
}

bool MappedInput::GetAction(const std::string &action, bool consume) {
    auto result = actions.find(action);
    if (result != actions.end()) {
        if (consume) {
            actions.erase(result);
        }

        return true;
    }

    return false;
}