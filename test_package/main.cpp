#include <core/game.hpp>
#include <iostream>

int main(int argc, char* argv[]) {
    Game game;
    game.Init(1280, 720);
    game.Run();
    return 0;
}